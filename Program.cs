﻿using System;

namespace CommandLineAnalog
{
    class Program
    {
        static void SuccesResponse(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        static void UnsuccessResponse(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        static void RenderMessage(string message)
        {
            if (message =="")
            {
                UnsuccessResponse("Given command doesn't exist");
            }
            else
            {
                SuccesResponse(message);
                Console.WriteLine();
            }
        }
        static void Main(string[] args)
        {
            string command = "";
            while (command != "exit")
            {
                Console.Write("Pleace enter commant to exacute : ");
                command = Console.ReadLine();
                string response = "";
                switch (command)
                {
                    case "username":response = Environment.UserName; break;
                    case "prosessor": response = Environment.ProcessorCount.ToString(); break;
                    case "os": response = Environment.OSVersion.VersionString; break;
                    case "machinename": response = Environment.MachineName; break;
                    case "systempagesize": response = Environment.SystemPageSize.ToString(); break;
                    case "tickcount": response = Environment.TickCount.ToString(); break;
                    case "userdomainname": response = Environment.UserDomainName; break;
                    case "systemdirectory": response = Environment.SystemDirectory; break;
                    case "version": response = Environment.Version.ToString(); break;
                    case "workingset": response = Environment.WorkingSet.ToString(); break;
                    case "userinteractive": response = Environment.UserInteractive.ToString(); break;
                    case "tickcount64": response = Environment.TickCount64.ToString(); break;
                    case "stacktrace": response = Environment.StackTrace.ToString(); break;
                    case "is64bitprosessor": response = Environment.Is64BitProcess.ToString(); break;
                    case "is64bitos": response = Environment.Is64BitOperatingSystem.ToString(); break;
                    case "hasshutdownstarted": response = Environment.HasShutdownStarted.ToString(); break;
                    case "currentDirectory": response = Environment.CurrentDirectory.ToString(); break;
                    case "currentmanagedthread": response = Environment.CurrentManagedThreadId.ToString(); break;
                    case "commandline": response = Environment.CommandLine.ToString(); break;


                        //case "newline": response = Environment.NewLine.ToString(); break;



                }

                RenderMessage(response);
            }

        }
    }
}
